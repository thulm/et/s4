# 4. Semester
## Inhalt
1. [SIGV_Signalverarbeitung](https://gitlab.com/thulm/et/s4/-/tree/main/ETRO_Elektronik_2)
2. [RTMA_Regelungstechnik_und_elektrische_Maschinen](https://gitlab.com/thulm/et/s4/-/tree/main/RTMA_Regelungstechnik_und_elektrische_Maschinen)
3. [ETRO_Elektronik_2](https://gitlab.com/thulm/et/s4/-/tree/main/SIGV_Signalverarbeitung)
4. [SOFEN_Software_Engineering](https://gitlab.com/thulm/et/s4/-/tree/main/SOFEN_Software_Engineering)

Für alle anderen Infos bitte die [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README.md) aus dem 1. Semester lesen!