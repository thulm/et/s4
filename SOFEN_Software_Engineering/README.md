# SOFEN_Software Engineering
Bitte Regeln im [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README.md) vom 1. Semester Beachten.

### Erfahrungen/Tipps @LorenzHund WS 21/22
#### Vorlesung
- Es wurde nur der erste Block für neuen Inhalt genutzt. Der zweite Block wurde für die Gruppenarbeit freigestellt
- Inhalt war gut strukturiert oft jedoch sehr viel für eine Vorlesungsstunde
#### Gruppenarbeit
- Gruppen werden gelost (bei uns 4 Gruppe)
- Es ging fast 4 Wochen bis die ersten Zeilen Code geschrieben wurden. Davor fanden viele Themen zur "Projektmanagement" innerhalb der SE Platz 
- Interessant und wichtig in dieser Zeit ist die Vorlesung zur Architektur. Lieber dort Zeit einsetzen als in die Requirements 
- Zu Beginn hat es sich bei uns bewährt erst die Architektur mit StateMachine zum Laufen zu bringen
- Implementierung der Funktionen und der Hardware kamen dadurch bei uns erst sehr spät, aber dafür dann sehr schnell 
- Aufwand gegen Mitte/Ende des Semester darf nicht unterschätzt werden
- UnitTests und auch Dokumentation kann viel Zeit benötigen
#### Klausur/Benotung
- Unser Semester kann da keine Referenz sein, da aufgrund des Hackerangriff kein Zugang auf Gitlab von außen möglich war, somit auch keine Benotung über das Repo möglich
- Benotung über Repo, Dokumentation, Präsentation und evtl. Wettkampf am Semesterende (nicht gesichert, lieber nachfragen)
- Prinzipiell sehr wohlwollend benotet

### Erfahrungen/Tipps 
#### Vorlesung
#### Gruppenarbeit
#### Klausur