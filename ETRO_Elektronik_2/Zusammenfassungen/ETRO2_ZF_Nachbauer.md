**Klausuraufgaben**

- Bandgap (4-1 / 4-2)	6 Aufgaben
- DA-Umsetzer
- OPV-Aufgabe (oder Rückkopplung)  → einfache Grundschaltungen (andere Aufgabe möglich: Rückkopplung)



| Begriff                          | Erklärung                                                    |
| -------------------------------- | ------------------------------------------------------------ |
| Rückkopplung                     | Ausgangssignal-Teil auf Eingang zurückgeführt                |
| Gegen-/Mit-Kopplung              | gegen-/gleichphasige Rückkopplung                            |
| Berechnung in Arbeitspunkt       | Vernachlässigen von (realer) Eingangs-Quelle und Ausgangslast |
| p- / n-Kanal MOSFET              | Pfeil nach außen / innen                                     |
| npn- / pnp-Transistor            | Pfeil nach außen / innen                                     |
| Bandgap-Referenz-Spannungsquelle | temperaturunabhängige Spannung: negativen Temperatur-<br />koeffizient mit positivem Temperaturkoeffizient von U~T~ |
| $A=\frac{B}{B+1}$                |                                                              |

| Leistung     | $x[dB]=10·\log\left(\frac{P_1}{P_2}\right)$ | $\frac{P_1}{P_2}=10^{\frac{x[dB]}{10}}$              | $\frac{0dB\quad3dB\quad6dB\quad10dB}{1\qquad2\qquad4\qquad10}$ |
| ------------ | ------------------------------------------- | ---------------------------------------------------- | ------------------------------------------------------------ |
| **Spannung** | $x[dB]=20·\log\left(\frac{U_1}{U_2}\right)$ | $\frac{U_1}{U_2}=10^{\left(\frac{x[dB]}{20}\right)}$ | $\frac{0dB\quad6dB\quad12dB\quad20dB}{1\qquad2\qquad4\qquad10}$ |



# 2. Operationsverstärker



### 2.1 Idealer OPV

| Verstärkung                                     | Strom            | Impedanz                                                  | Eigenschaften                       | Phase                         |
| ----------------------------------------------- | ---------------- | --------------------------------------------------------- | ----------------------------------- | ----------------------------- |
| $V_0→\infty\\V_{gl}=\dfrac{u_A}{(u_p+u_n)/2}=0$ | $i_p=0A\\i_n=0A$ | $Z_p=Z_n=\infty~\Omega\\Z_D=\infty~\Omega\\ Z_A=0~\Omega$ | frequenz- und temperatur-unabhängig | $\varphi(u_A)-\varphi(u_D)=0$ |



|                                                              |                                                              |                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Invertierend<br /><br />$V_u=\dfrac{u_A}{u_E}=-\dfrac{R_2}{R_1}$ | ![image-20210317102730549](../../Typora.assets/image-20210317102730549.png) | Nicht-Invertierend<br /><br />$V_u=\dfrac{u_A}{u_E}=1+\dfrac{R_2}{R_1}$ | ![image-20210328120231052](../../Typora.assets/image-20210328120231052.png) |
| Subtrahierer<br /><br />$\dfrac{u_A}{u_{E1}-u_{E2}}=\dfrac{R_2}{R_1}$ | ![image-20210328120202877](../../Typora.assets/image-20210328120202877.png) | Addierer                                                     | $\displaystyle u_A=-R·\sum_{i=1}^n\dfrac{u_{Ei}}{R_i}$       |
| Spannungsfolger                                              | $V_u=\dfrac{u_A}{u_E}=1$                                     | Integrierer<br /><br />$\displaystyle u_A(t)=-\dfrac1{RC}\int u_E(t)~dt$ | ![image-20210328120320382](../../Typora.assets/image-20210328120320382.png) |
| Differenzierer<br /><br />$u_A(t)=-RC~\dot u_E(t)$           | ![image-20210328120347681](../../Typora.assets/image-20210328120347681.png) | Differenzierer Praktisch<br />$f\ll\dfrac1{2\pi CR_1}\\~\\u_A(t)=-RC~\dot u_E(t)$ | ![image-20210328120523515](../../Typora.assets/image-20210328120523515.png) |



### 2.2 Realer OPV

| Gleichtakt-Eingangsspannung              | Gleichtakt-Verstärkung            | Gleichtakt-Unterdrückung   |
| ---------------------------------------- | --------------------------------- | -------------------------- |
| $u_{gl}=\dfrac{u_p+u_n}{2}=\dfrac{u_D}2$ | $V_{gl}=\dfrac{u_{A,gl}}{u_{gl}}$ | $CMRR=\dfrac{V_0}{V_{gl}}$ |

| Eingangs-Ruhestrom                                           | Eingangs-Offset-Strom               | Eingangs-Offset-Spannung                                |
| ------------------------------------------------------------ | ----------------------------------- | ------------------------------------------------------- |
| $I_B=\dfrac{I_p+I_n}2\bigg\vert_{U_n=U_p=0V}$                | $I_{OS}=I_p-I_n\bigg\vert_{U_A=0V}$ | $U_{OS}=U_p-U_n\bigg\vert_{u_A=0V,~\frac{U_p+U_n}2=0V}$ |
| wird kompensiert, wenn $R_{Q,p}=R_{Q,n}$<br />$R_{Q,p}=R_{BIAS}=R_1\parallel R_2$ | $u_A=-I_{OS}·R_2$                   |                                                         |
| <img src="../../Typora.assets/image-20210318101725730.png" alt="image-20210318101725730" style="zoom:33%;" /> |                                     |                                                         |



# 3. Rückkopplung

<img src="../../Typora.assets/image-20210328121539128.png" alt="image-20210328121539128" style="zoom: 33%;" />

| Verstärkung                                             | Betriebsverstärkung                                          | <u>V</u>~0~ → ∞                         | Rückkopplungs-Grad                           |
| ------------------------------------------------------- | ------------------------------------------------------------ | --------------------------------------- | -------------------------------------------- |
| $\underline V_0=\dfrac{\underline x_a}{\underline x_i}$ | $\underline V_B=\dfrac1{\frac1{\underline V_0}-\underline k}=\dfrac{\underline V_0}{\underline g}$ | $\underline V_B=-\dfrac1{\underline k}$ | $\underline g=1-\underline k·\underline V_0$ |

|                                                              |                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210328124620139](../../Typora.assets/image-20210328124620139.png) | ![image-20210328124639533](../../Typora.assets/image-20210328124639533.png) | ![image-20210328124722496](../../Typora.assets/image-20210328124722496.png) |

| Schwankung  <u>V</u>~0~                                      | Bandbreite                                                   | $\underline V_{B0}=\dfrac{\underline V_0}{1-k·\underline V_0}$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\dfrac{\Delta\underline V_B}{\underline V_B}=\dfrac{\Delta\underline V_0}{\underline V_0}·\dfrac1{1-\underline k·\underline V_0}$ | $\underline V_B(jf)=\dfrac{\underline V_{B0}}{1+j\frac{f}{f_{Bg}}}$ | $f_{Bg}=f_{0g}·(1-·\underline V_0)$                          |
| relative Änderung Betriebsverstärkung bei relativer Änderung von Leerlaufverstärkung | 0: niedrige Frequenz                                         | g: 3dB-Grenzfrequenz                                         |

<img src="../../Typora.assets/image-20210328131356153.png" alt="image-20210328131356153" style="zoom:33%;" />



### 3.2 Rückkopplungsarten

| Typ                     | Eingangs-Widerstand                         | Ausgangs-Widerstand                         | Einheit               |
| ----------------------- | ------------------------------------------- | ------------------------------------------- | --------------------- |
| u u (Serien-Parallel)   | $Z_e=Z_i·(1-k·V_0)$                         | $Z_a=\dfrac{u_a}{i_a}=\dfrac{Z_o}{1-k·V_0}$ | $[V_B]=1$             |
| i i (Parallel-Serien)   | $Z_e=\dfrac{u_e}{i_e}=\dfrac{Z_i}{1-k·V_0}$ | $Z_a=Z_o·(1-k·V_0)$                         | $[V_B]=1$             |
| u i (Serien-Serien)     | $Z_e=Z_i·(1-k·V_0)$                         | $Z_a=Z_o·(1-k·V_0)$                         | $[V_B]=\dfrac1\Omega$ |
| i u (Parallel-Parallel) | $Z_e=\dfrac{u_e}{i_e}=\dfrac{Z_i}{1-k·V_0}$ | $Z_a=\dfrac{u_a}{i_a}=\dfrac{Z_o}{1-k·V_0}$ | $[V_B]=\Omega$        |



| Reale Serien-Parallel-Rückkopplung$~\\\vert h_{21}\vert\ll\vert V_0·\dfrac{Z_i}{Z_o}\vert\\~\\\begin{pmatrix}u_1\\i_2\end{pmatrix}=\begin{pmatrix} h_{11}&h_{12}\\h_{21}&h_{22}\end{pmatrix}\begin{pmatrix}i_1\\u_2\end{pmatrix}$ | ![image-20210329101918789](../../Typora.assets/image-20210329101918789.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210329104831730](../../Typora.assets/image-20210329104831730.png) | $h_{11}=\dfrac{u_1}{i_1}\bigg\vert_{u_2=0V}\quad h_{12}=\dfrac{u_1}{u_2}\bigg\vert_{i_1=0A}\quad h_{21}=\dfrac{i_2}{i_1}\bigg\vert_{u_2=0V}\quad h_{22}=\dfrac{i_2}{u_2}\bigg\vert_{i_1=0A}$ |
| $V_0^*=\dfrac{u_a}{u_i^*}\qquad k^*=-h_{12}\\~\\V_B^*=\dfrac{u_a}{u_e}=\dfrac{V_0^*}{1-k^*·V_0^*}\\~\\~\\Z'_e=Z_e^*·(1-k^*·V_0^*)\\~\\Z_a'=\dfrac{Z_a^*}{1-k^*·V_0^*}$ | ![image-20210329101941731](../../Typora.assets/image-20210329101941731.png) |



| Reale Parallel-Serien-Rückkopplung $~\\\vert g_{21}\vert\ll\vert V_0·\dfrac{Z_o}{Z_i}\vert\\~\\\begin{pmatrix}i_1\\u_2\end{pmatrix}=\begin{pmatrix} g_{11}&g_{12}\\g_{21}&g_{22}\end{pmatrix}\begin{pmatrix}u_1\\i_2\end{pmatrix}$ | ![image-20210329104548963](../../Typora.assets/image-20210329104548963.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210329103915679](../../Typora.assets/image-20210329103915679.png) | $g_{11}=\dfrac{i_1}{u_1}\bigg\vert_{i_2=0A}\quad g_{12}=\dfrac{i_1}{i_2}\bigg\vert_{u_1=0V}\quad g_{21}=\dfrac{u_2}{u_1}\bigg\vert_{i_2=0A}\quad g_{22}=\dfrac{u_2}{i_2}\bigg\vert_{u_1=0V}$ |
| $V_0^*=\dfrac{i_a}{i_i^*}\qquad k^*=-g_{12}\\~\\V_B^*=\dfrac{u_a}{u_e}=\dfrac{V_0^*}{1-k^*·V_0^*}\\~\\~\\Z_e'=\dfrac{Z_e^*}{1-k^*·V_0^*}\\~\\Z'_a=Z_a^*·(1-k^*·V_0^*)$ | ![image-20210329105205701](../../Typora.assets/image-20210329105205701.png) |



| Reale Serien-Serien-Gegenkopplung $~\\\vert Z_{21}\vert\ll\vert V_0·Z_i·Z_o\vert\\~\\\begin{pmatrix}u_1\\u_2\end{pmatrix}=\begin{pmatrix} Z_{11}&Z_{12}\\Z_{21}&Z_{22}\end{pmatrix}\begin{pmatrix}i_1\\i_2\end{pmatrix}$ | ![image-20210329110119555](../../Typora.assets/image-20210329110119555.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210329110149861](../../Typora.assets/image-20210329110149861.png) | $Z_{11}=\dfrac{u_1}{i_1}\bigg\vert_{i_2=0A}\quad Z_{12}=\dfrac{u_1}{i_2}\bigg\vert_{i_1=0A}\quad Z_{21}=\dfrac{u_2}{i_1}\bigg\vert_{i_2=0A}\quad Z_{22}=\dfrac{u_2}{i_2}\bigg\vert_{i_1=0A}$ |
| $V_0^*=\dfrac{i_a}{u_i^*}\qquad k^*=-Z_{12}\\~\\V_B^*=\dfrac{u_a}{u_e}=\dfrac{V_0^*}{1-k^*·V_0^*}\\~\\~\\Z'_e=Z_e^*·(1-k^*·V_0^*)\\~\\Z_a'=Z_a^*·(1-k^*·V_0^*)$ | ![image-20210329110653537](../../Typora.assets/image-20210329110653537.png) |



| Reale Parallel-Parallel-Gegenkopplung $~\\\vert Y_{21}\vert\ll\vert \dfrac{V_0}{Z_i·Z_o}\vert\\~\\\begin{pmatrix}i_1\\i_2\end{pmatrix}=\begin{pmatrix} Y_{11}&Y_{12}\\Y_{21}&Y_{22}\end{pmatrix}\begin{pmatrix}u_1\\u_2\end{pmatrix}$ | ![image-20210329111000129](../../Typora.assets/image-20210329111000129.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210329111126482](../../Typora.assets/image-20210329111126482.png) | $Y_{11}=\dfrac{i_1}{u_1}\bigg\vert_{u_2=0V}\quad Y_{12}=\dfrac{i_1}{u_2}\bigg\vert_{u_1=0V}\quad Y_{21}=\dfrac{i_2}{u_1}\bigg\vert_{u_2=0V}\quad Y_{22}=\dfrac{i_2}{u_2}\bigg\vert_{u_1=0V}$ |
| $V_0^*=\dfrac{u_a}{i_i^*}\qquad k^*=-Y_{12}\\~\\V_B^*=\dfrac{u_a}{u_e}=\dfrac{V_0^*}{1-k^*·V_0^*}\\~\\Z_e'=\dfrac{Z_e^*}{1-k^*·V_0^*}\\~\\Z'_a=\dfrac{Z_a^*}{1-k^*·V_0^*}$ | ![image-20210329111440759](../../Typora.assets/image-20210329111440759.png) |



| U  (V~0~ · i~i~)    ⏀                           | Ｉ  (V~0~ · u~i~)    ⊖                                 | U  (V~0~ · u~i~)    ⏀                      | Ｉ  (V~0~ · i~i~)    ⊖                     |  Bedingung   |
| ----------------------------------------------- | ------------------------------------------------------ | ------------------------------------------ | ------------------------------------------ | :----------: |
| $V_0=\dfrac{u_a}{i_i}\bigg\vert_{i_a=0}=\Omega$ | $V_0=\dfrac{i_a}{u_i}\bigg\vert_{u_a=0}=\dfrac1\Omega$ | $V_0=\dfrac{u_a}{u_i}\bigg\vert_{i_a=0}=1$ | $V_0=\dfrac{i_a}{i_i}\bigg\vert_{u_a=0}=1$ | $i_2\ll i_a$ |



### 3.3 Mitkopplung

| Stabil                                                       | Instabil                                                     |      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
| $\Im\{-\underline k·\underline V_0\}=0\quad\&\quad-k·V_0>-1$ | $\Im\{-\underline k·\underline V_0\}=0\quad\&\quad-k·V_0<-1$ |      |
|                                                              |                                                              |      |

| Mitkopplung                | Gegenkopplung                 |
| -------------------------- | ----------------------------- |
| $\varphi(\omega_0)=2n·\pi$ | $\varphi(\omega_0)=(2n+1)\pi$ |



#### Ringoszillator

$f_0=\dfrac1{T_0}=\dfrac1{2nt_d}$



# 4. OPV Aufbau



### 4.1.1 Stromquellen mit Bipolartransistoren

| aktiver Vorwärtsbetrieb                                      | Stromverstärkung                 | Ausgangswiderstand     |
| ------------------------------------------------------------ | -------------------------------- | ---------------------- |
| $I_C=I_S·e^{\frac{U_{BE}}{U_T}}·\left(1+\frac{U_{CE}}{U_A}\right)$ | $I_C=B·I_B\quad/\quad=\beta·I_B$ | $r_o=\dfrac{U_A}{I_C}$ |

| Name                                                         | Bild                                                         | U~CE1~ = U~CE2~                                              | U~CE1~ ≠ U~CE2~                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Einfacher Bipolar-Stromspiegel**                           | ![image-20210414124613383](../../Typora.assets/image-20210414124613383.png) | $I_o=I_{REF}\left(1-\frac2{B+2}\right)\\~\\\dfrac{I_o-I_{REF}}{I_{REF}}\approx-\dfrac2B\\~\\~\\I_o=\dfrac{I_{REF}}{1+\frac{n+1}B}$ | $I_o\approx I_{C1}\left(1+\frac{\Delta U_{CE}}{U_A}\right)\\~\\\quad\approx I_{C1}+\dfrac{\Delta U_{CE}}{r_{o2}}\\~\\R_o=r_{o2}=\dfrac{U_A}{I_{C2}}$ |
| **Stromspiegel Emitter-gegenkoppl.**<br />Verringerung Umsetzungs-fehler (Offset<br /> + GND) | ![image-20210415121155976](../../Typora.assets/image-20210415121155976.png) | ‘’                                                           | $I_o\approx I_{C1}+\dfrac{\Delta U_{CE}}{R_{o2}}\\~\\\overset{I_{REF}R_{E1}\gg U_T}\approx I_{REF}·\dfrac{R_{E1}}{R_{E2}}\\~\\R_o=r_{o2}(1+g_{m2}R_{E2})$ |
| **Sromspiegel Basisstrom-Verstärkung**                       | ![image-20210415122037270](../../Typora.assets/image-20210415122037270.png) | $I_o=\dfrac{I_{REF}}{1+\frac2{B·(B+1)}}\\~\\\dfrac{I_o-I_{REF}}{I_{REF}}\approx-\dfrac2{B^2}\\~\\~\\I_o=\dfrac{I_{REF}}{1+\frac{n+1}{B·(B+1)}}$ | wie einfacher Bipolar-Stromspiegel                           |
| **Widlar-Stromquelle**<br />kleine Ströme                    | ![image-20210419085016210](../../Typora.assets/image-20210419085016210.png) | $U_T\ln\left(\frac{I_{REF}}{I_o}\right)=I_oR_{E2}$           |                                                              |
| **Wilson-Stromquelle**                                       | ![image-20210419092300805](../../Typora.assets/image-20210419092300805.png) | $I_o=I_{REF}-\frac{2·I_{REF}}{B^2+2B+2}\\~\\\dfrac{I_o-I_{REF}}{I_{REF}}\approx-\dfrac2{B^2}\\~\\~\\R_o=\dfrac{B·r_{o2}}2$ |                                                              |



### 4.1.2 Stromquellen mit MOSFETs

| Sättigungsbereich  U~DS~ ≥ U~GS~ – U~TH~                     | Ströme                 | Ausgangswiderstand                         |
| ------------------------------------------------------------ | ---------------------- | ------------------------------------------ |
| $I_D=\dfrac{k_nW}{2~L}(U_{GS}-U_{TH})^2(1+\lambda U_{DS})$   | $I_B=0\qquad I_D=-I_S$ | $r_o=\dfrac1{I_D\lambda}=\dfrac{U_A}{I_D}$ |
| $U_{GS}\overset{\lambda=0}=U_{TH}\pm\sqrt{\dfrac{I_D·2·L}{k_{n/p}}}$ |                        |                                            |

| Name                              | Bild                                                         | U~DS1~ = U~DS2~                                              | U~DS1~ ≠ U~DS2~                                              |
| --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Einfacher MOSFET-Stromspiegel** | ![image-20210419093806041](../../Typora.assets/image-20210419093806041.png) | $I_o=I_{REF}\dfrac{L_1W_2}{W_1L_2}$<br /><br />+ kleinstes U~o,min~ <br />– kleiner R~o~ | $I_o=\frac{L_1W_2}{W_1L_2}(I_{REF}+\frac{\Delta U_{DS}}{R_o})\\~\\R_o=r_{o2}=\dfrac1{I_o\lambda}$ |
| **MOSFET-Kaskode-Stromspiegel**   | ![image-20210419094543470](../../Typora.assets/image-20210419094543470.png) | $U_{o,min,Sät}=\\U_{GS1}+U_{GS4}-U_{TH}\\~\\R_o=\dfrac1{I_o\lambda}+\dfrac{g_{m3}}{(I_o\lambda)^2}\\~\\~\\g_{m3}=\sqrt{2k_n\dfrac{W_3}{L_3}I_o}$ | – größtes U~o,min~ <br />+ großer R~o~                       |
| **MOSFET-Kaskode-**               | **Stromspiegel mit ver-größertem Aussteuerbereich**          |                                                              | + mittleres U~o,min~ <br />– großer R~o~                     |



## 4.2 Stabilisierungsschaltungen

R~REF~  ersetzt  I~REF~:	**Einfacher Stromspiegel**	$I_o∼U_{CC}$	**Widlar-Stromquelle**	$U_T\ln\left(\frac{U_{CC}}{R_{REF}·I_o}\right)=I_oR_{E2}$

| Name                                                         | Bild                                                         | Formel                                                       |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **U~BE~-Referenzquelle**<br /><br />$I_o=\frac{U_T}{R_{E2}}\\·\ln\left(\frac{U_{CC}-U_{BE1}-U_{BE2}}{R_{REF}·I_{S1}}\right)$ | ![image-20210422150035570](../../Typora.assets/image-20210422150035570.png) | **Selbstversorgende<br />U~BE~ -Referenz**<br /><br />$I_{C2}=\dfrac{U_T}{R_{E2}}·\ln\left(\dfrac{I_{REF}}{I_{S1}}\right)\\~\\~\\T_4=T_5:\quad I_{C2}=I_{REF}$ | ![image-20210422151016863](../../Typora.assets/image-20210422151016863.png) |
| **Zener Spannungs-Referenz**<br /><br />$\dfrac{\partial U_o}{\partial T}=0\dfrac{mV}K$ | ![image-20210422151813682](../../Typora.assets/image-20210422151813682.png) | **Selbstversorgende<br />Strom-Referenz**<br /><br />$\dfrac{\partial U_R}{\partial T}=0\dfrac{mV}K\\~\\I_o=I_R=\dfrac{U_R}R\\~\\\dfrac{\partial I_o}{\partial T}=\dfrac{\partial U_R}{R·\partial T}=0\dfrac{mA}K$ | ![image-20210422152027924](../../Typora.assets/image-20210422152027924.png) |
| **Widlar-Bandabstands-Referenz**<br /><br />$\Delta U_{BE}=U_T\ln(\frac{R_2}{R_1})\\~\\U_{R2}=\Delta U_{BE}·\dfrac{R_2}{R_3}\\~\\U_{G0}=1.205V\\~\\U_o\bigg\vert_{T=300K}\!\!=1.262V$ | ![image-20210422152240528](../../Typora.assets/image-20210422152240528.png) | **Verbesserte<br />Bandabstands-<br />Referenz-<br />Spannungsquelle**<br /><br />$\dfrac{R_2}{R_1}=\dfrac{I_{C1}}{I_{C2}}\\~\\\Delta U_{BE}=U_T·\ln\left(\dfrac{R_2}{R_1}\right)$ | ![image-20210422163153149](../../Typora.assets/image-20210422163153149.png) |
| $\dfrac{I_1}{I_2}=\dfrac{R_2}{R_1}$                          | $U_o=U_{BE3}+U_T~\frac{R_2}{R_3}~\ln\left(\frac{R_2I_{S2}}{R_1I_{S1}}\right)$ |                                                              | $U_o=U_{BE1}+U_T~\frac{R_2}{R_3}~\ln\left(\frac{R_2}{R_1}\right)\\\overset{Band-\\~~gap}=1.262V$ |

<div style="page-break-after: always; break-after: page;"></div>

## 4.3 Verstärker mit aktiver Last

| Näherung aktiver Vorwärtsbetrieb                             | $U_{BE}>0,\quad U_{CB}>0\\ U_{BE}\gg U_T$ | $I_C=I_S·e^{\frac{U_{BE}}{U_T}}\quad I_B=\dfrac{I_S}{B_F}·\left(e^{\frac{U_{BE}}{U_T}}-1\right)\approx\dfrac{I_C}{B_F}$ |
| ------------------------------------------------------------ | ----------------------------------------- | ------------------------------------------------------------ |
|                                                              | npn:  $N_D\gg N_A,\\ A_F\approx1$         | $I_S=A·q·n_i^2·\dfrac{D_n}{W_B·N_A}\approx I_{ES}$           |
| **mit Early-Effekt**<br />Erhöhung von U~CE~ & U~CB~ und dadurch I~C~ | $U_{CE}\approx U_{CB}$                    | $I_C=I_S·e^{\frac{U_{BE}}{U_T}}\left(1+\dfrac{U_{CB}}{U_A}\right)\approx I_S·e^{\frac{U_{BE}}{U_T}}\left(1+\dfrac{U_{CE}}{U_A}\right)$ |
| **Aktiver Inversbetrieb**                                    | $B_R\approx\dfrac1{100}·B_F$              | $B_F$  (Vorwärts)    /    $B_R$  (Invers)                    |
| **Faktor**                                                   | $A=\dfrac{B}{1+B}$                        | $B=\dfrac{A}{1-A}\qquad\eta=\dfrac{U_T}{U_A}$                |

| Steigung (Leitwert)                                          | Kleinsignalstrom                                             | Verstärkung                                      |                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------ | --------------------------- |
| $g_m=\dfrac{i_c}{u_{be}}=\frac{dI_C}{dU_{BE}}=\dfrac{I_C}{U_T}$ | $i_c=\dfrac{I_C}{U_T}·u_{be}\qquad i_C=I_C·e^{\frac{u_{be}}{U_T}}$ | $\beta=\dfrac{i_c}{i_b}\quad B=\dfrac{I_C}{I_B}$ | $r_\mu=\dfrac{u_{ce}}{i_b}$ |
| $u_{be}\ll U_T$                                              | $U_{be}\ll U_T$                                              | $\beta=B\quad(B\ const)$                         | $r_\mu\ge r_o·\beta$        |

| Eingangswiderstand                             | Basis-Diffusionskapazität                                    | Ausganswiderstand                                            |
| ---------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $r_\pi=\dfrac{u_{be}}{i_b}=\dfrac{\beta}{g_m}$ | $C_D=\dfrac{q_L}{u_{be}}=\dfrac{\Delta Q_L}{\Delta U_{BE}}=\tau_F·g_m$ | $r_o=\dfrac{u_{ce}}{i_c}=\dfrac{U_A}{I_C}=\dfrac{U_T}{\eta·I_C}=\dfrac1{\eta·g_m}=\dfrac1{g_o}$ |

|                                                         | <img src="../../Typora.assets/image-20210509091916926.png" alt="image-20210509091916926" style="zoom:33%;" /> | $u_o=\dfrac{(U_{CC}-\vert u_{BE3}\vert)·U_{An}}{U_{An}+U_{Ap}}\\\qquad+U_{A,\text{eff}}\left(1-\dfrac{B_p+2}{B_p}\dfrac{I_{S1}}{I_{REF}}e^{\frac{u_i}{U_T}}\right)\\~\\ U_{A,\text{eff}}=\dfrac{U_{An}·U_{Ap}}{U_{An}+U_{Ap}}\\~\\ V_U(i_{c1}=I_{C3})=\dfrac{-1}{\eta_n+\eta_p}\qquad \eta=\dfrac{U_T}{U_A}$ |
| ------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Differenz-Verstärker**                                | <img src="../../Typora.assets/image-20210511102335693.png" alt="image-20210511102335693" style="zoom:33%;" /> | $V_{Ud}=g_mR_C\qquad g_m=\dfrac{I_{EE}}{2U_T}\\V_{ud}=\dfrac{U_{CC}}{2U_T}$ |
| **Modifizierter Differenz-Verstärker mit aktiver Last** | <img src="../../Typora.assets/image-20210511103527636.png" alt="image-20210511103527636" style="zoom:33%;" /> | $\dfrac{i_{C1}}{i_{C2}}=e^{\frac{u_{id}}{U_T}}\qquad\overset{B→\infty}{i_{C4}=i_{C3}=-i_{C1}}\\~\\i_o\overset{R_L→0\Omega} =A·I_{EE}\tanh\left(\dfrac{u_{id}}{2U_T}\right)\\~\\u_o=U_{CC}-u_{BE3}\bigg\vert_{\vert i_{c3}\vert=\frac{I_{EE}}2}\\\qquad\qquad+2U_{A,\text{eff}}·\tanh\left(\dfrac{u_{id}}{2U_T}\right)\\U_{A,\text{eff}}=U_{An}\parallel U_{Ap}$ |
| **Offset-spannung**                                     | <img src="../../Typora.assets/image-20210511110234793.png" alt="image-20210511110234793" style="zoom:33%;" /> | $I_{C1}=-I_{C3}\dfrac{B_p+2}{B_p}\\~\\\Delta U_{BE}=U_{OS}=U_T·\ln\left(\dfrac{B_p+2}{B_p}\right)$ |

| Widerstand                                        | Strom/Steilheit                                       | Spannung                             |                              |
| ------------------------------------------------- | ----------------------------------------------------- | ------------------------------------ | ---------------------------- |
| $R_{id}=\dfrac{2\beta_n}{g_m}∼\dfrac1{I_{EE}}$    | $i_o=g_m·u_{id}$                                      | $V_{Ud}=g_m(R_o\parallel R_L)$       | $V_{gl}=0$                   |
| $R_o=\dfrac1{g_m(\eta_n+\eta_p)}∼\dfrac1{I_{EE}}$ | $G_m=\dfrac{i_o}{u_{id}}\bigg\vert_{R_L=0\Omega}=g_m$ | $V_{U0}=\dfrac1{g_m(\eta_n+\eta_p)}$ | ${\scriptstyle CMRR}=\infty$ |

| Modifizierter Differenz-Verstärker mit aktiver Last in CMOS-Technologie | <img src="../../Typora.assets/image-20210511111244180.png" alt="image-20210511111244180" style="zoom:33%;" /> | $\beta=k_n·\dfrac WL \\~\\i_o=\beta·u_{id}\sqrt{\dfrac{I_{SS}}\beta-\dfrac{u_{id}^2}4}\\~\\ u_{id}\bigg\vert_{i_o=I_{SS}}=\sqrt2·U_{OV}$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |



## 4.5 Frequenzgangs-Kompensation

| Polverschiebung                              | Polaufsplittung                                              |
| -------------------------------------------- | ------------------------------------------------------------ |
| für alle ohmschen<br />Rückkopplungen stabil | Bandbreite des kompensierten Verstärkers größer als bei Polverschiebung<br />höheres Verstärkung-Bandbreite-Produkt |

| Polstellen       | Leerlaufverstärkung bei p~2~ = 0dB                           |      |
| ---------------- | ------------------------------------------------------------ | ---- |
| $p=-\dfrac1{RC}$ | $\vert p_1'\vert=\dfrac{\vert p_2\vert}{\vert V_0\vert}\qquad C_{K1}=\dfrac{-1}{p_1'R_1}-C_1$ |      |

<div style="page-break-after: always; break-after: page;"></div>

# 5. A/D- und D/A-Umsetzer

| Kleinste Spannungsstufe                | Idealfall    $0\le q<U_{LSB}$            | Ausgangsspannung |
| -------------------------------------- | ---------------------------------------- | ---------------- |
| $U_{LSB}=\dfrac{U_{max}-U_{min}}{2^n}$ | $Z·U_{LSB}\le u_i<(Z+1)·U_{LSB}$         | $u_o=U_{LSB}·Z$  |
| n:  Anzahl Bits                        | Quantisierungsfehler:  $q=u_i-Z·U_{LSB}$ |                  |

| Unipolare Umsetzer                                        | Bipolare Umsetzer                                            |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| $0\le U_{min}\le u_i,~u_o\le U_{max}\qquad0\le Z\le2^n-1$ | $0>U_{min}\le u_i,~u_o\le U_{max}\qquad -2^{n-1}\le Z\le 2^{n-1}-1$ |

| Differential Non-Linearity              | Integral Non-Linearity                   |
| --------------------------------------- | ---------------------------------------- |
| $DNL[n]=\dfrac{u[n+1]-u[n]}{U_{LSB}}-1$ | $INL[n]=\dfrac{u[n]-u[0]}{U_{LSB}}-Z[n]$ |

| Least Significant Bit      | % of Full Scale Range        | Parts Per Million of Full Scale Range |
| -------------------------- | ---------------------------- | ------------------------------------- |
| $U_{LSB}=\dfrac{FSR}{2^n}$ | $\%FSR=\dfrac{Z}{2^n}·100\%$ | $ppmFSR=\dfrac{Z}{2^n}·1~000~000$     |

| Rauschleistung                                       | Signalleistung       | Signal to Noise Ratio                                        |
| ---------------------------------------------------- | -------------------- | ------------------------------------------------------------ |
| $P_{R,id}=\dfrac{2^{-2n}}3$<br />(Auf P~S~ normiert) | $P_{Sinus}=\dfrac12$ | $SNR_{id}=(n·6.02+1.76)dB\overset{P_N\ge P_{N,id}}=10·\log\left(\dfrac1{2P_N}\right)dB$ |

| Signal to Noise and Distortion                           | Signal to Noise and Distortion Ratio              | Effective Number Of Bits            |
| -------------------------------------------------------- | ------------------------------------------------- | ----------------------------------- |
| $SINAD=10·\log\left(\dfrac{P_S+P_N+P_D}{P_N+P_D}\right)$ | $SNDR=10·\log\left(\dfrac{P_S}{P_N+P_D}\right)dB$ | $ENOB=\dfrac{SINAD-1.76dB}{6.02dB}$ |



## 5.2 A/D-Umsetzungsverfahren

| Name                                    | Formel                                 | Vorteile                                                     | Nachteile                                                    |
| --------------------------------------- | -------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Zählverfahren                           | Vergleichszyklen<br />= 2^n-1^         | sehr geringer Schaltungsaufwand<br />große Genauigkeit       | Langsame Umsetzung                                           |
| Kompensations-verfahren                 |                                        | geringer Schaltungsaufwand<br />große Genauigkeit            | schnelle Umsetzung nur<br />bei langsamen Änderungen         |
| Dual-Slope-Verfahren                    | $Z=b=\dfrac{\overline u_i}{U_{ref}}·a$ | Vorteile von Komp-Verfahren<br />geringer Aufwand zur Takterzeugung | sehr langsam<br />nur konstante $u_i$ (Voltmeter)            |
| Wägeverfahren<br />(sukzessive Approx.) | Vergleichszyklen<br />= Anzahl bits    | geringer Schaltungsaufwand<br />große Genauigkeit<br />moderate Umsetzungsgeschw. | Abtast-Halte-Glied<br />zwingend notwendig                   |
| Parallelverfahren                       | Komperatoren<br />= 2^n^ – 1           | schnellste Umsetzung                                         | hoher Schaltungsaufwand<br />hohe Komperator-Anforder.<br />hoher Leistungsverbrauch |
| Erweitertes<br />Parallelverfahren      |                                        | schnelle Umsetzung<br />geringerer Aufwand als Parall.verf.  | moderater Schaltungsaufw.<br />hohe Komperator-Anforder.     |
| Delta-Sigma-Umsetzer                    | $OSR=\dfrac{f_a}{2f_{S,max}}$          | $SNR_{min}=10\log(\frac{3(2n+1)}{2\pi^{2n}}OSR^{2n+1})$      | n:  Ordnung ADC (nicht bit)                                  |



## 5.3 D/A-Umsetzungsverfahren

| Name                                              | Skizze                                                       | Formel                                                       |
| ------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Zählverfahren                                     | ![image-20210711121035789](../../Typora.assets/image-20210711121035789.png) | $\dfrac{t_{on}}T=\dfrac{u_o}{u_{o,max}}=\dfrac{Z}{Z_{max}}$  |
| Wägeverfahren<br />(gewichtete<br />Widerstände)  | ![image-20210711120632491](../../Typora.assets/image-20210711120632491.png) | $u_{o,max}=-U_{REF}\dfrac{2^n-1}{2^n}\dfrac{2R_K}R\\~\\\left\vert \dfrac{\Delta U_{MSB}}{U_{MSB}} \right\vert\le2^{-n}$ |
| Wägeverfahren<br />(R2R Netzwerk)                 | ![image-20210711115847019](../../Typora.assets/image-20210711115847019.png) | $u_{o,max}=-U_{REF}\dfrac{2^n-1}{2^n}\dfrac{R_K}R$           |
| Wägeverfahren<br />(gewichtete<br />Stromquellen) | ![image-20210711120554520](../../Typora.assets/image-20210711120554520.png) | $u_{o,max}=i_{o,max}·R_L\\~\\=(2^n-1)·I_0·R_L$               |
| Wägeverfahren<br />(gewichtete<br />Kapazitäten)  | ![image-20210711115800257](../../Typora.assets/image-20210711115800257.png) | $u_{o,max}=U_{REF}·\dfrac{2^n-1}{2^n}$                       |





































## Fehler

- R_a und R_e nicht vergessen
- Vierpol Parameter: Zahlenwerte berechnen
- V_0 oft negativ, da  u~o~ = – g~m~ · u~i~ · R~a~^*^   und oft Kilo...
- Wenn U~A~ → ∞ , dann gilt (wenn keine Emitter-Widerstände)  I~C1~ = I~C2~
- 



