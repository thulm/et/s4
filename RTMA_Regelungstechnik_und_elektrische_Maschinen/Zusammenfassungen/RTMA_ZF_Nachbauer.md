## 0. Wiederholung Systemtheorie

**Fehler**

- Nyquist-Kriterium → G~K~ stabil → “-2” darf nicht umschlungen werden → stabil

| Eingang                                                      | System | Ausgang                              |
| ------------------------------------------------------------ | ------ | ------------------------------------ |
| - Steuer-/Stellgrößen<br />- Störgrößen                      | DGL    |                                      |
| - periodische Signale<br />- instationäre Signale<br />- Testsignal |        | - Impulsantwort<br />- Sprungantwort |

​				$\dfrac1{\omega_0^2}~\ddot y+\dfrac{2D}{\omega_0}~\dot y+y=V·x$				<img src="../../Typora.assets/image-20201203115741498.png" alt="image-20201203115741498" style="zoom:40%;" />

| D____________________ | Eigenschaft                 | Eigenwerte                                                   | Beispiel                          |                                                              |
| --------------------- | --------------------------- | ------------------------------------------------------------ | --------------------------------- | ------------------------------------------------------------ |
| **( -∞ ; -1 ]**       | instabil<br />aperiodisch   | $s_{1,2}=-\omega_0D\pm \omega_0\sqrt{D^2-1}$                 | $s_{1,2}=+x~~\xcancel{+j·y}$      | <img src="../../Typora.assets/image-20201205081809443.png" alt="image-20201205081809443" style="zoom:33%;" /> |
| **( -1 ; 0 ]**        | instabil<br />periodisch    | $s_{1,2}=-\omega_0D\pm j·\omega_0\sqrt{1-D^2}$               | $s_{1,2}=+x\pm j·y$               | <img src="../../Typora.assets/image-20201205081755456.png" alt="image-20201205081755456" style="zoom:33%;" /> |
| **D = 0**             | grenzstabil<br />periodisch | $s_{1,2}=~\pm j·\omega_0\sqrt{1-D^2}$                        | $s_{1,2}=\xcancel{\pm x}~\pm j·y$ | <img src="../../Typora.assets/image-20210129122943203.png" alt="image-20210129122943203" style="zoom:33%;" /> |
| **[ 0 ; 1 )**         | stabil<br />periodisch      | $s_{1,2}=-\omega_0D\pm j·\omega_0\sqrt{1-D^2}$               | $s_{1,2}=-x\pm j·y$               | <img src="../../Typora.assets/image-20201205081844258.png" alt="image-20201205081844258" style="zoom:33%;" /> |
| **[ 1 ; ∞ )**         | stabil<br />aperiodisch     | $s_{1,2}=-\omega_0D\pm\omega_0\sqrt{D^2-1}=\dfrac1{T_{1,2}}$ | $s_{1,2}=-x~~\xcancel{+j·y}$      | <img src="../../Typora.assets/image-20201205081558577.png" alt="image-20201205081558577" style="zoom:33%;" /> |

​		$a\ddot x+b\dot x+cx=0$				**Resonanzfrequenz:**	$\omega_0=\sqrt{\dfrac ca}$				**Dämpfung:**	$D=\dfrac{b}{2\sqrt{ac}}$

<div style="page-break-after: always; break-after: page;"></div>

## 1. Regelkreis

| Leistung     | $x[dB]=10·\log\left(\frac{P_1}{P_2}\right)$ | $\frac{P_1}{P_2}=10^{\frac{x[dB]}{10}}$              | $\frac{0dB\quad3dB\quad6dB\quad10dB}{1\qquad2\qquad4\qquad10}$ |
| ------------ | ------------------------------------------- | ---------------------------------------------------- | ------------------------------------------------------------ |
| **Spannung** | $x[dB]=20·\log\left(\frac{U_1}{U_2}\right)$ | $\frac{U_1}{U_2}=10^{\left(\frac{x[dB]}{20}\right)}$ | $\frac{0dB\quad6dB\quad12dB\quad20dB}{1\qquad2\qquad4\qquad10}$ |

| <img src="../../Typora.assets/image-20210325091033191.png?lastModify=1623396177" alt="image-20210325091033191"  /> | <img src="../../Typora.assets/image-20210331102913986.png" alt="image-20210331102913986" style="zoom: 67%;" /> |
| ------------------------------------------------------------ | ------------------------------------------------------------ |

| Bezeichnung––––––––––                                        | Übertragungsfunktion                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210331112302260](../../Typora.assets/image-20210331112302260.png) | ![image-20210331112150932](../../Typora.assets/image-20210331112150932.png) |
| **geschlossener Regelkreis**                                 | $G_g(s)=T(s)=\dfrac{Y(s)}{W(s)}=\dfrac{G_{vor}(s)}{1~{\color{red}\mp}~G_{vor}·G_{rück}}=\dfrac{K·G_S·G}{1\mp KG_SG·G_M}$ |
| **Störgröße 1**                                              | $G_{d1}(s)=\dfrac{Y(s)}{D_1(s)}=\dfrac{G(s)}{1+GG_MKG_S}$    (Plus, weil in Beispiel-Kreis Minus) |
| **Störgröße 2**                                              | $G_{d2}(s)=\dfrac{Y(s)}{D_2(s)}=\dfrac{1}{1+G_MKG_SG}$       |
| **Störgröße 3**                                              | $G_N(s)=\dfrac{Y(s)}{N(s)}=\dfrac{KG_SG}{1+KG_SG·G_M}$       |
| **offener Kreis**                                            | $G_K(s)=K·G_S·G·G_M$    (für alle ÜF identisch, Stabilitäts-Kriterium) |

$\displaystyle\lim_{t→\infty} y(t)=\lim_{s→0}s·Y(s)=\lim_{s→0}s·X(s)·G(s)\overset{falls~Sprung\\}=\lim_{s→0}G(s)$	(nur bei stabilen Strecken)

<div style="page-break-after: always; break-after: page;"></div>

### 1.4 Anforderungen an Regelkreis

| Anforderung        | Beschreibung                                                 |
| ------------------ | ------------------------------------------------------------ |
| Stabilität         | meistens sowieso gegeben                                     |
| Führungsverhalten  | Regelgröße folgt Führungsgröße bei Nutzfrequenz ($0\le\omega\le\omega_g$) $\quad T(j\omega)=\dfrac{Y(j\omega)}{W(j\omega)}\approx 1$ |
| Störverhalten      | Störunterdrückung bei konstantem Sollwert $\quad\vert G_d(j\omega)\vert=\vert\dfrac{Y(j\omega)}{D(j\omega)}\vert\approx0$ |
| Robustheit         | Parameterschwankungen / Modellunsicherheiten                 |
| Stellgrößeneinsatz | Systemgrenzen einhalten, Rauschen nicht verstärken, begrenzter Energieeinsatz |

Gutes Führungsverhalten + Gutes Störverhalten = stationäre Genauigkeit



### 1.5 Auslegung von Regelkreis

| Betragsoptimum                                | Zeitpunkt Maximum                     | Überschwingweite 0 < D < 1                | Dämpfung                                                     |
| --------------------------------------------- | ------------------------------------- | ----------------------------------------- | ------------------------------------------------------------ |
| $D=\dfrac1{\sqrt2}\quad(\ddot u~\hat=~4,3\%)$ | $t^*=\dfrac\pi{\omega_0\sqrt{1-D^2}}$ | $\ddot u=e^{\frac{-\pi D}{\sqrt{1-D^2}}}$ | $D=\sqrt{\dfrac{(\ln\ddot u)^2}{\pi^2+(\ln\ddot u)^2}}=\cos(\vartheta)$ |

|                              | 0 < D < 1                                                    | 1 << D                                                       |
| ---------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Zeitpunkt eingeschwungen** | $T_S=\dfrac{-\ln(\varepsilon)}{\omega_0D}\quad\pm\varepsilon:$Zielkorridor | $T_S=\dfrac{\ln(\varepsilon)}{\sigma_{slow}}=\dfrac{\ln(\varepsilon)}{-\omega_0D\left(1-\sqrt{1-\frac1{D^2}}\right)}$ |
| Beispiel  $\pm2\%$           | $T_S\approx\dfrac4{\omega_0D}$                               | $T_S\approx\dfrac{8D}{\omega_0}$                             |

|                              V                               |                              D                               |                             ω~0~                             |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| ![image-20210401092928576](../../Typora.assets/image-20210401092928576.png) | ![image-20210401093020307](../../Typora.assets/image-20210401093020307.png) | ![image-20210401092949080](../../Typora.assets/image-20210401092949080.png) |
|                                                              |                        **Amplitude**                         |                          **Phase**                           |
|                      Frequenzbereich :                       | ![image-20210401093040589](../../Typora.assets/image-20210401093040589.png) | ![image-20210401093058614](../../Typora.assets/image-20210401093058614.png) |

<div style="page-break-after: always; break-after: page;"></div>  

### 1.6 Stationäre Genauigkeit

| Anforderung                                                  | Reglerauslegung    (Integrator am besten ganz vorne)       |
| ------------------------------------------------------------ | ---------------------------------------------------------- |
| Führungsverhalten    $\displaystyle \lim_{s→0}\frac{Y(s)}{W(s)}=1$ | Integrator im Vorwärtspfad (der nicht gekürzt werden kann) |
| Störverhalten    $\displaystyle \lim_{s→0}\frac{Y(s)}{D(s)}=0$ | Integrator muss vor Störung eingebaut sein                 |



## 2. Reglertypen

| Typ                    |                                                              | K (s)                                                        |
| ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **P** ~(proportional)~ | <img src="../../Typora.assets/image-20210406214031164.png" alt="image-20210406214031164" style="zoom:25%;" /> | $V_R$                                                        |
| **I** ~(Integral)~     | <img src="../../Typora.assets/image-20210406214048240.png" alt="image-20210406214048240" style="zoom:25%;" /> | $\dfrac1{T_is}$                                              |
| **P I**                | <img src="../../Typora.assets/image-20210406214107444.png" alt="image-20210406214107444" style="zoom:25%;" /> | $V_R~\dfrac{T_is+1}{T_is}\qquad T_i=V_R·\widetilde T_i$      |
| **P D (T)**            | <img src="../../Typora.assets/image-20210611101716194.png" alt="image-20210611101716194" style="zoom:25%;" /> | ${V_R~\dfrac{T_Ds+1}{T_D's+1}}\qquad T_D=\frac{\widetilde T_D}{V_R}$ |
| **P I D**              | <img src="../../Typora.assets/image-20210611101853427.png" alt="image-20210611101853427" style="zoom:25%;" /> | $V_R·\dfrac{T_is+1}{T_is}·\dfrac{T_Ds+1}{\widetilde{T_D} s+1}\qquad$ |
| **P T 1**              | <img src="../../Typora.assets/image-20201119185412076.png?lastModify=1611913386" alt="image-20201119185412076" style="zoom:25%;" /> | $\dfrac{V}{Ts+1}$                                            |

| System        | Dämpfung    $D^2=\frac{b^2}{4·a·c}$            |
| ------------- | ---------------------------------------------- |
| I · PT~1~     | $D^2=\dfrac{T_i}{4·T·V}$                       |
| PT~1~ · PT~1~ | $D^2=\dfrac{(T_1+T_2)^2}{4·T_1T_2·(1+V_1V_2)}$ |

<div style="page-break-after: always; break-after: page;"></div>

## 3. Reglerauslegung



### 3.2.1 Pol-/Nullstellenkompensation

| PI                                                           | PD                                                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210612171537070](../../Typora.assets/image-20210612171537070.png) | ![image-20210612171552323](../../Typora.assets/image-20210612171552323.png) |
| größte ZK kompensieren                                       | zweitgrößte (kleinere) ZK kompensieren                       |

Pol-/Nullstellen-Kompensation nur für Führungsgröße schneller, Störgröße langsam

keine instabilen Pole kompensieren



### 3.2.2 Sensor-Übertragungsfunktion  $G_M(s)\ne1$

| G~K~ ≠ G~V~                                                  | $G_M\ne1\qquad\widetilde{G_g}=\frac{G_K}{1+G_K}\qquad G_g(s)=\widetilde{G_g}(s)·\dfrac1{G_M(s)}$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210612172954071](../../Typora.assets/image-20210612172954071.png) | ![image-20210612173008700](../../Typora.assets/image-20210612173008700.png) |
| ![image-20210612173842568](../../Typora.assets/image-20210612173842568.png) | ![image-20210612173905795](../../Typora.assets/image-20210612173905795.png) |
| **höheren Überschwinger vermeiden:**<br />Führungs-Filter:  $G_F=G_M$ | ![image-20210612174118876](../../Typora.assets/image-20210612174118876.png) |

<div style="page-break-after: always; break-after: page;"></div>  

### 3.2.3 Methode Ersatzzeitkonstante

> stationäre Genauigkeit: Integral + kein Überschwinger: D=1
>
> Ersatzzeitkonstanten werden (außer bei Express) nicht kompensiert

| faktorisierte Form                                  | gebrochen rationale Form                      | Ersatz                                                       |
| --------------------------------------------------- | --------------------------------------------- | ------------------------------------------------------------ |
| $G(s)=\dfrac{V_1}{T_1s+1}·\dfrac{V_2}{…}·e^{-T_Ls}$ | $G(s)=\dfrac{…b_1s+b_0}{…a_1s+a_0}·e^{-T_Ls}$ | $G_e(s)=\dfrac{V_e}{(\frac{T_e}ns+1)^n}$                     |
| $T_e=\sum_iT_i+\sum_jT_{Lj}$                        | $T_e=\dfrac{a_1}{a_0}+T_L-\dfrac{b_1}{b_0}$   | $T_e=\dfrac{A_R}{y_\infty}$    <img src="../../Typora.assets/image-20210717174643332.png" alt="image-20210717174643332" style="zoom:33%;" /> |
| $V_e=\prod_iV_i$                                    | $ V_e=\dfrac{b_0}{a_0}$                       | $V_e=\dfrac{y_\infty}{w_\infty}$                             |

|                                        | Bild                                                         | Kompens.                                              | Formeln                                                      |
| -------------------------------------- | ------------------------------------------------------------ | ----------------------------------------------------- | ------------------------------------------------------------ |
| **Standard**<br />$K(s)=\dfrac1{T_is}$ | ![image-20210612175333347](../../Typora.assets/image-20210612175333347.png) | keine                                                 | $T_i=4D^2·V_e·T_e\\\widetilde{T_e}=4D^2·T_e\\~\\G_g(s)=\dfrac1{\widetilde{T_e}s+1}$ |
| **Express**<br />P I                   | ![image-20210612175428468](../../Typora.assets/image-20210612175428468.png) | $T_i=\frac{T_e}2$                                     |                                                              |
| **Express**<br />P I D                 | $G_V=G_K=\dfrac{V_R·V_3}{T_is~(T_D's+1)·(\frac{T_e}3s+1)}$   | $T_i=\frac{T_e}3\\T_D'=\frac{T_D}{10}=\frac{T_e}{30}$ | $G_e=\dfrac{V_e}{(\frac{T_e}3s+1)^3}$                        |
| **Teilweise**<br />P I                 | ![image-20210612180932650](../../Typora.assets/image-20210612180932650.png) | $T_i=T_1$<br />(langsam)                              | $T_e=T_2+T_3…$                                               |
| **Teilweise**<br />P I D               | ![image-20210612181216648](../../Typora.assets/image-20210612181216648.png) | $T_i=T_1\\T_D=T_2$<br />(langsam)                     |                                                              |

| Standard / Express                                           | Teilweise Kompensation                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1. eine EZK ersetzt gesamte Strecke<br />2. ggf. wird EZK bei Reglerauslegung kompensiert (nur Express) | 1. ZK bei Reglerauslegung kompensieren<br />2. Rest mit EZK vereinfachen |



<div style="page-break-after: always; break-after: page;"></div>  

## 4. Stabilität & “Sicherheitsabstand”



### 4.2 Betrags- und Phasenabstand

| Ortskurve  $G_K$                                             | Ortskurve  $G_K$                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210613121554445](../../Typora.assets/image-20210613121554445.png) | ![image-20210613123653724](../../Typora.assets/image-20210613123653724.png) |
| Vorraussetzung:  G~K~ und G~g~ stabil                        | Frequenz:    $\vert G_K(j\omega_d)\vert=1$                   |
| $\vert G_K(j\omega_\pi)\vert\le1\qquad\log(\vert G_K(j\omega_\pi)\vert)<0\\$ | $\varphi(\omega_\pi)=-\pi\qquad\Psi_d=\pi+\varphi(\omega_d)\\$ |
| **Bode-Diagramm**  $G_K$                                     | **Betragsabstand (Gain Margin)**                             |
| ![image-20210613123721602](../../Typora.assets/image-20210613123721602.png) | ![image-20210613123721602](../../Typora.assets/image-20210613123721602 (Kopie).png) |
| $GM=\dfrac1{\vert G_K(j\omega_\pi)\vert}=\dfrac1{r_\pi}\quad\bigg/=20·\log\left(\frac1{\vert G_K(j\omega_\pi)\vert}\right)$ | $r_\pi=1+G_K(j\omega_\pi)\approx\dfrac1{GM}$                 |



### 4.3 Symmetrisches Optimum

| Regler                                                       | Zeitkonstante    | Optimum                                         | Verstärkung                   |
| ------------------------------------------------------------ | ---------------- | ----------------------------------------------- | ----------------------------- |
| $G_K(s)=V_{PI}\dfrac{T_{PI}~s+1}{T_{PI}~s}·\dfrac{V_1}{T_1s+1}·\dfrac1{T_is}$ | $T_1<T_{PI}$     | $\omega_m=\omega_d=\sqrt{\omega_{PI}·\omega_1}$ | $V_{PI}=\dfrac{T_i}{V_1T_1a}$ |
| PI · PT1 · I    /    PI · IT1                                | $T_{PI}=a^2·T_1$ | $\vert G_K(j\omega_d)\vert=1$                   | $D=\frac{a-1}2$               |

<div style="page-break-after: always; break-after: page;"></div>

### 4.1 Nyquist-Kriterium

| einfach                                                      | geschlossener Regelkreis                                     | stabiles_System___                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------ |
| ![image-20210613102256014](../../Typora.assets/image-20210613102256014.png) | ![image-20210613102118639](../../Typora.assets/image-20210613102118639.png) | Pole  $G_g$  /  Nullstellen  $1+G_K$ |
| $G_g(s)=\dfrac{G_K(s)}{1+G_K(s)}$                            | $G_g(s)=\dfrac{G_V(s)}{1+G_K(s)}$                            | keine in rechter s-HE ($m=0$)        |

| Umschlingungen im Uhrzeigersinn:    $m-n$    (stabil: m=0) | instabile Polstellen:    $n$      |
| ---------------------------------------------------------- | :-------------------------------- |
| **von 0:**    $1+G_K(s)$        **von -1:**    $G_K(s)$    | **instabile Nullstellen:**    $m$ |





## 5. Elektrische Maschinen

| Name                    | Elektrisch             | Magnetisch                                                   |
| ----------------------- | ---------------------- | ------------------------------------------------------------ |
| Strom                   | $I=\dfrac UR=G·U$      | $\Phi=\dfrac\Theta{R_m}=\Lambda_m·\Theta\qquad\Phi=B·A\qquad [\Phi]=Wb$ |
|                         | $T=\dfrac LR$          | Flussverkettung:    $\Psi=N·\Phi=L·I$                        |
| Extra                   | $[L]=1H=1\frac{Vs}A$   | $H=\dfrac B\mu\qquad \mu_r=\dfrac B{\mu_0H}\qquad \mu_0=4\pi·10^{-7}\frac Hm$ |
| Energie /<br />Leistung | $P=\dfrac{U^2}R=I^2·R$ | $W_m=\dfrac12·\dfrac{N^2·I^2}{\mu_0·\delta}·A_{Fe}$          |

| Lorentzkraft                                                 | Reluktanzkraft                                    | Induktion                                                    |
| ------------------------------------------------------------ | ------------------------------------------------- | ------------------------------------------------------------ |
| $F=Q·v·B·\sin(\alpha)=I·l·B$                                 | $F_R=-\mu_0·\dfrac{N^2·I^2·A_{Fe}}{2·\delta^2}$   | $u_L=\Psi_{max}\cos(\varphi)·\omega\\=N·B·2rl·\cos(\varphi)·\omega$ |
| $\alpha→\vec v~\sphericalangle~\vec B$<br />Kraft kann Anziehen und Abstoßen | Kraft kann nur Anziehen<br />(Stromrichtung egal) | $\varphi→A~\sphericalangle~\vec B$                           |

<div style="page-break-after: always; break-after: page;"></div>

## 6. Die Gleichstrommaschine (GSM)

| Betrieb                                     | Motor                                                        | Generator (Bremsen)                                          |
| ------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Skizze                                      | ![image-20210710104145652](../../Typora.assets/image-20210710104145652.png) | ![image-20210710104210313](../../Typora.assets/image-20210710104210313.png) |
| Wirkungsgrad                                | $\eta=\dfrac{P_{mech}}{P_{el}}=\dfrac{P_{el}-\sum_iP_{V,i}}{P_{el}}$ | $\eta=\dfrac{P_{el,A}}{P_{mech}~(+P_{el,E})}$                |
| Leistungsfluss                              | ![image-20210717113205903](../../Typora.assets/image-20210717113205903.png) | ![image-20210717113232247](../../Typora.assets/image-20210717113232247.png) |
| Vorzeichen<br />Leistung  $P$<br />Quadrant | $\omega_M\quad M_M$    gleich<br />> 0 (+)<br />I & III      | $\omega_M\quad M_M$    gegensätzlich<br />< 0 (–)<br />II & IV |

| Nennpunkt:  M~i,N~                                           | ———————————————————                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20210717104035929](../../Typora.assets/image-20210717104035929.png) | $\dfrac{M_i}{I_A}$  Bis Nennpunkt:<br />+ Nebenschluss<br />+ Fremderregt<br /><br />$\dfrac{M_i}{I_A}$  Ab Nennpunkt:<br />+ Reihenschluss |

<img src="../../Typora.assets/image-20210718102121005.png" alt="image-20210718102121005" style="zoom: 33%;" />

<div style="page-break-after: always; break-after: page;"></div>

|          | Anker (elektrisch) | Innen               | Rotor (mechanisch)     |
| -------- | ------------------ | ------------------- | ---------------------- |
| Spannung | $U_A=U_{R_A}+U_i$  | $U_i=\Psi_E·\omega$ |                        |
| Leistung | $P_A=P_{R_A}+P_i$  | $P_i=M_i·\omega$    | $P_i-P_V=P_N=P_{mech}$ |
| Moment   |                    | $M_i=\Psi_E·I_A$    | $M_i-M_V=M_N$          |

| Wirkungsgrad                  | Schlupf                                                      | Drehzahl                       | Leerlauf                       | Maximal                                   |
| ----------------------------- | ------------------------------------------------------------ | ------------------------------ | ------------------------------ | ----------------------------------------- |
| $\eta=\dfrac{P_{ab}}{P_{zu}}$ | $s=\dfrac{\omega_0-\omega_M}{\omega_0}=\dfrac{n_0-n_M}{n_0}$ | $\omega=\dfrac{2\pi}{60sec}·n$ | $\omega_0=\dfrac{U_A}{\Psi_E}$ | $M_A=\dfrac{U_A\Psi_E}{R_A}=\dfrac{M_i}s$ |

| Name / ω~M~                                                  | Skizze                                                       | Graph                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Fremderregt $~\\\dfrac{U_A}{\Psi_E}-\dfrac{R_A~(+R_V)}{{\Psi_E}^2}·M_i$ | ![image-20210710095341409](../../Typora.assets/image-20210710095341409.png) | ![image-20210717103737405](../../Typora.assets/image-20210717103737405.png) |
| Nebenschluss $~\\\dfrac{\tilde R_E}{L_E}-\dfrac{\tilde R_A{\tilde R_E}^2}{(L_EU_A)^2}M_i\\~\\\tilde R_E=R_E+R_F\\\tilde R_A=R_A+R_V$ | ![image-20210710095539815](../../Typora.assets/image-20210710095539815.png) | ![image-20210717103413539](../../Typora.assets/image-20210717103413539.png) |
| Reihenschluss $~\\\dfrac{U_A}{\sqrt{M_i·L_E}}-\dfrac{\tilde R}{L_E}\\~\\~\\\tilde R=R_A+R_E+R_V$ | ![image-20210710095613198](../../Typora.assets/image-20210710095613198.png) | ![image-20210717103516083](../../Typora.assets/image-20210717103516083.png) |
| Doppelschluss<br /><br />+ Generator (last-unabhänige Spannung) | ![image-20210710095641217](../../Typora.assets/image-20210710095641217.png) | ![image-20210717103617106](../../Typora.assets/image-20210717103617106.png) |
