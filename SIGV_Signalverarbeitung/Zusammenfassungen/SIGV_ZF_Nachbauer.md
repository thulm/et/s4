| Erzeugen | Bearbeiten | Auswerten |
|----------|------------|-----------|
| Modulation | Filterung | Identifizierung |
| DTO (???) | Spektralanalyse | Klassifizierung |
| Zufallsgeneratoren | Demodulation | Decodierung |
|  | Signalschätzung |  |

## 2\. Signal-Abtastung und -Rekonstruktion

| $f=\\dfrac\\omega{2\\pi}$ | Frequenzgang | Impulsantwort |
|------------------------|--------------|---------------|
| `Rechteck-Puls` | $F_W(f)=rect\\left(\\dfrac{f}{2f_g}\\right)$ | $2f_g·si(\\pi2f_gt)$ |
| `Halteglied` | $F_H(f)=e^{-j\\pi f\\Delta T}·si(\\pi f\\Delta T)$ |  |

| $u(m\\Delta T)·rect\\left(\\dfrac{t-m\\Delta T}\\tau\\right)·\\dfrac\\tau{\\Delta T}$ |  | $2f_g·si(\\pi2f_gt)\\quad○—!●\\quad rect(\\frac{f}{2f_g})$ |
|------------------------------------------------------------------------------|--|--------------------------------------------------------|
| ![image-20210324115340571](../../Typora.assets/image-20210324115340571.png?lastModify=1626168985) | ![image-20210316174707526](../../Typora.assets/image-20210316174707526.png) | ![image-20210316174730587](../../Typora.assets/image-20210316174730587.png) |

| Abgetastetes Signal |  |
|---------------------|--|
| $\\displaystyle u^*(t)=u(t)·Ш\_{\\Delta T}(t)=\\sum\_{m=-\\infty}^{+\\infty}u(m\\Delta T)·\\delta(t-m\\Delta T)$ | $\\displaystyle U^*(f)=Ш\_{\\Delta T}\\circledast U(f)=\\sum\_{k=-\\infty}^\\infty U\\left(f-\\dfrac{k}{\\Delta T}\\right)$ |

### 2\.2 Abtast-Theoreme

| Tiefpassignal | Bandpassignal |
|---------------|---------------|
| $X(0<f<f_g)$ | $X(f_1<\\vert f\\vert<f_2)$ |
| $f\_{\\Delta T}=\\dfrac1{\\Delta T}>2f_g\\~\\X(f>f_g)=0\\$<br />Tiefpass-Signale sind Signale, die die Frequenz f=0 einschließen und auf einen Bereich 0 ≤ f ≤ f g begrenzt sind. | maximale Anzahl an Bändern zwischen 0 und f~1~ |

$\\begin{matrix}f\_{\\Delta T}>2(f_2-f_1)\\~\\\\dfrac{2f_2}{\\lambda+1}\\lt f\_{\\Delta T}\\lt\\dfrac{2f_1}\\lambda\\\\lambda\_{max}=\\left[\\dfrac{f_1}{f_2-f_1}\\right]\\end{matrix}\\qquad\\begin{array}{c|c|c}\\lambda&\\dfrac{2f_2}{\\lambda+1}&\\dfrac{2f_1}\\lambda\\\\hline \\lambda_3\\\\lambda_2\\\\lambda_1\\end{array}\\$

### 2\.3 Signal-Rekonstruktion

| Tiefpass | Frequenzgang | Rekonstruktion $U(f)=U^*(f)·F(f)$ |
|----------|--------------|-----------------------------------|
| **Whittaker** $f_g=\\frac1{2\\Delta T}$ | $F_W(f)=rect\\left(\\dfrac{f}{2f_g}\\right)$ | $\\displaystyle○—!●\\quad u(t)=\\sum\_{m=-\\infty}^\\infty u(m\\Delta T)·si\\left(\\pi~\\dfrac{t-m\\Delta T}{\\Delta T}\\right)$ |
| **Halteglied** | $F_H(f)=si(\\pi f\\Delta T)·e^{-j\\pi f\\Delta T}$ |  |

### 2\.4 [Quadratur](https://youtu.be/h_7d-m1ehoY)\-Signalverarbeitung

| Analytisches Signal/Spektrum | Hilbert-Transformator | Amplitudengang | Phasengang |
|------------------------------|-----------------------|----------------|------------|
| $\\qquad\\quad x^+(t)=x(t)+j~u(t)\\○—!●\~\~ X^+(t)=X(f)+j~U(f)$ | $H(f)=\\dfrac{U(f)}{X(f)}=-j·sgn(f)$ | ![image-20210324115652054](../../Typora.assets/image-20210324115652054.png) | ![image-20210324115709986](../../Typora.assets/image-20210324115709986.png) |

minimale Abtast-Frequenz:	$f\_{\\Delta t}\\ge f_2-f_1$

| Einseitenband-Modulation |
|--------------------------|
| $x\_{ESB}(t)=x(t)·\\cos(\\omega_0t)-u(t)·\\sin(\\omega_0t)$ |

| RF (Radio Frequenzy) | LO (Local Oscillatior) | Dopplerverschiebung | Dopplerauflösung |
|----------------------|------------------------|---------------------|------------------|
| Modulation | Demodulation | $f_D=2·\\dfrac vc·f\_{RF}$ | $f_D=\\dfrac1T$ |

<div style="page-break-after: always; break-after: page;"></div>

## 3\. Spektralanalyse (45-)

| x (t) | X (f) | X (jω) | X (z) |
|-------|-------|--------|-------|
| $\\delta(t-m\\Delta T)$ | $\\Delta T·e^{-j2\\pi~m\\Delta T·f}$ |  |  |
| $\\varepsilon(t)$ |  |  | $\\Delta T\\dfrac{z}{z-1}$ |
| $Ш\_{\\Delta T}(t)$ | ${\\displaystyle\\sum\_{k=-\\infty}^{+\\infty}}\\delta\\left(f-\\frac{k}{\\Delta T}\\right)=\\dfrac{1sec}{\\Delta T}·Ш\_{\\frac1{\\Delta T}}(f)$ |  |  |
| $\\cos(2\\pi f_0·t)$ | $\\dfrac12·~\\big[\\delta(f+f_0)+\\delta(f-f_0)\\big]$ | $\\pi·~\\big[\\delta(\\omega+\\omega_0)+\\delta(\\omega-\\omega_0)\\big]$ |  |
| $\\sin(2\\pi f_0·t)$ | $\\dfrac j2·~\\big[\\delta(f+f_0)-\\delta(f-f_0)\\big]$ | $j\\pi·~\\big[\\delta(\\omega+\\omega_0)-\\delta(\\omega-\\omega_0)\\big]$ |  |

#### 3\.1.1 Fourierreihe

| Reelle Darstellung | Komplexe Darstellung |
|--------------------|----------------------|
| $\\displaystyle x(t)=x_0+\\sum\_{k=1}^\\infty~a_k\\cos(k~\\omega_0t)+b_k\\sin(k~\\omega_0t)$ | $\\displaystyle x(t)=\\dfrac12\\sum\_{k=-\\infty}^\\infty~\\tilde x_k·e^{j~k~\\omega_0t}$ |
| $\\displaystyle a_k=\\dfrac2T\\int_T x(t)·\\cos(k~\\omega_0t)\\text{ dt}\\~\\\\displaystyle b_k=\\dfrac2T\\int_T x(t)·\\sin(k~\\omega_0t)\\text{ dt}$ | $\\displaystyle \\tilde x_k=a_k-jb_k=\\dfrac2T\\int_T x(t)·e^{-jk\\omega_0t}\\text{ dt}\\~\\\\displaystyle =\\dfrac2T\\int_T x(t)·\\big(\\cos(k\\omega_0t)-j\\sin(k\\omega_0t) \\big)\\text{ dt}$ |
| $\\displaystyle x_0=\\dfrac1T\\int_Tx(t)~dt$ | $\\tilde x_0=2·x_0$ |

| a~k~ ≠ 0 , b~k~ = 0 | a~k~ = 0 , b~k~ ≠ 0 | a~k~ ≠ 0 , b~k~ ≠ 0 |
|---------------------|---------------------|---------------------|
| gerades Signal (Bsp. cos)<br />reelles Spektrum | ungerades Signal (Bsp. sin)<br />imaginäres Spektrum | kein (un)gerades Signal<br />komplexes Spektrum |

| Signal | Gerade | Ungerade |
|--------|--------|----------|
| **Reell** | gerades, reelles Spektrum | ungerades, imaginäres Spektrum |
| **Imaginär** | gerades, imaginäres Spektrum | ungerades, reelles Spektrum |

#### 3\.1.3 Diskrete Fourier-Transformation

| Diskret : $\\displaystyle x^*(t)=x(t)·Ш\_{\\Delta T}(t)\\quad○—!●\\quad X^*(f)=\\Delta T·\\sum_kX_k=\\sum\_{i=-\\infty}^\\infty U\\left(f-\\dfrac{i}{\\Delta T}\\right)$ |
|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| **DFT** $\\displaystyle X_k=\\sum\_{m=0}^{N-1}~x(m~\\Delta T)·e^{-j2\\pi\\frac{km}N}$ **IDFT** $\\displaystyle{x(m~\\Delta T)=\\dfrac1N\\sum\_{k=0}^{N-1}}~X\\left(\\frac kT\\right)·e^{j2\\pi\\frac{km}N}$ |

|  | Fensterbreite | Abtast-Auflösung | Index | Variable |
|--|---------------|------------------|-------|----------|
| **Zeit** t , m | $T=N·\\Delta T$ | $\\Delta T=\\dfrac1{f\_{\\Delta T}}$ | $0\\le m\\le N-1$ | $t=m·\\Delta T$ |
| **Frequenz** f , k | $f\_{\\Delta T}=N·\\Delta f$ | $\\Delta f=\\dfrac1T$ | $0\\le k\\le N-1$ | $f=k·\\Delta f$ |

| Slot | 1 | 2 | 3 | N |  | $k+1$ | $\\frac f{\\Delta f}+1$ |
|------|---|---|---|---|--|-------|-----------------------|
| **k** | 0 | 1 | 2 | N – 1 |  | $k$ | $\\frac f{\\Delta f}$ |
| **f** | 0 · Δf | 1 · Δf | 2 · Δf | f~ΔT~ – Δf |  | $k·\\Delta f$ | $f$ |

| Periodisches Eingangssignal | $\\vert X(f_1)\\vert =A·\\frac N2$ | 1\. Slot: | $f_1=f_0$ | $k_1=\\frac{f_1}{\\Delta f}$ |
|-----------------------------|---------------------------------|----------|-----------|----------------------------|
| $x(t)=A·\\sin(2\\pi f_0~t)+d$ | $\\vert X(0)\\vert=N·d$ | **2\. Slot:** | $f_2=f\_{\\Delta T}-f_0$ | $k_2=\\frac{f_2}{\\Delta f}=N-k_1$ |

| Eigenschaften Signal | Eigenschaften Spektrum |
|----------------------|------------------------|
| \- zeitlich begrenzt<br />- periodisch wiederholt<br />- Fensterbreite | \- unbegrenzt, kontinuierlich<br />- diskret<br />- Frequenzauflösung |

| periodische Signale | Transiente Signale | Stochastische Signale |
|---------------------|--------------------|-----------------------|
| Hamming-Fenster, um leakage zu vermeiden | Rechteck-Fenster, da Ränder =0 | Fensterung von 20-50ms Länge |

### 3\.2 Rauschsignale

| Leistung | $x[dB]=10·\\log\\left(\\frac{P_1}{P_2}\\right)$ | $\\frac{P_1}{P_2}=10^{\\frac{x[dB]}{10}}$ | $\\frac{0dB\\quad3dB\\quad6dB\\quad10dB}{1\\qquad2\\qquad4\\qquad10}$ |
|----------|---------------------------------------------|-----------------------------------------|----------------------------------------------------------------|
| **Spannung** | $x[dB]=20·\\log\\left(\\frac{U_1}{U_2}\\right)$ | $\\frac{U_1}{U_2}=10^{\\left(\\frac{x[dB]}{20}\\right)}$ | $\\frac{0dB\\quad6dB\\quad12dB\\quad20dB}{1\\qquad2\\qquad4\\qquad10}$ |

| stationärer Prozess | ergodischer Prozess |
|---------------------|---------------------|
| Statistik ändert sich nicht | Schar-Mittelwert = zeitlichem Mittelwert eines Exemplars $\\widetilde{s(t_1)}=\\overline{^ks(t)}$ |

| Dichte | Verteilung | mehrdimensional |
|--------|------------|-----------------|
| $p_N(n)\\qquad\\int p_N(n)dn=1$ | $P_N(n<n_0)=\\int\_{-\\infty}^{n_0}p_N(n)~dn$ | $p\_{N_1N_2}(n_1,n_2)=p\_{N_1}(n_1)·p\_{N_2}(n_2)$ |
| **Streuung** | **Mittelwert** | **quadratischer Mittelwert** |
| $\\sigma^2=s_x^2-m_x^2\\~\\\\sigma^2=\\overline{n^2}-(\\overline n)^2\\~\\ =E(N^2)-E(N)^2$ | $\\displaystyle E(N)=\\overline n=\\int\_{-\\infty}^\\infty n·p_N(n)~dn\\~\\\\displaystyle =\\overline{n(t)}=\\lim\_{T→\\infty}\\dfrac1{2T}\\int\_{-T}^Tn(t)dt$ | $\\displaystyle E(N^2)=\\int\_{-\\infty}^\\infty n^2·p_N(n)~dn\\~\\\\displaystyle =\\overline{n^2(t)}=\\lim\_{T→\\infty}\\dfrac1{2T}\\int\_{-T}^T n^2(t)~dt$ |
| Wechselleistung | Gleichanteil | normierte mittlere Leistung |

|  | Signal |  |  | Spektrum |
|--|--------|--|--|----------|
| **Signal** | $n(t)$ | $n(\\tau)$ | $n(-\\tau)$ | $\\displaystyle \\varphi\_{nn}(\\tau)=\\lim\_{T→\\infty}\\dfrac1{2T}·n(-\\tau)\\circledast n(\\tau)$ |
| **Spektrum** | $N(\\omega)$ | $N(f)$ | $N^*(f)$ | $\\displaystyle\\Phi\_{nn}(\\omega)=\\lim\_{T→\\infty}\\dfrac1{2T}·\\vert N(\\omega)\\vert^2$ |

| Energie | Leistung |
|---------|----------|
| $\\displaystyle E_x=\\int\_{-\\infty}^\\infty x^2(t)~dt$ | $\\displaystyle P_x=\\dfrac1{2T}\\int\_{-T}^Tx^2(t)~dt$ |
| $\\displaystyle E_x=\\int\_{-\\infty}^\\infty\\vert X(f)\\vert^2~df=\\dfrac1{2\\pi}\\int\_{-\\infty}^\\infty\\vert X(\\omega)\\vert^2~d\\omega$ | $\\displaystyle P_x=\\int\_{-\\infty}^\\infty\\Phi\_{xx}(f)~df= \\dfrac1{2\\pi}\\int\_{-\\infty}^\\infty\\Phi\_{nn}(\\omega)~d\\omega$ |
|  | $\\int\\sin^2(x)dx=\\dfrac12(x-\\frac12\\sin(2x))$ |
| $S\_{xx}^E(\\omega)=\\vert X(\\omega)\\vert^2$ | $x(t)=\\sin(\\omega t)\\quad→\\quad P=\\dfrac12$ |

|  | Signal | Spektrum | $\\varphi\_{hh}(\\tau)\\quad○—!●\\quad\\vert H(f)\\vert^2$ |
|--|--------|----------|-----------------------------------------------------|
| terministisch | $n(t)\\circledast h(t)=g(t)$ | $\\Phi\_{nn}(f)·\\vert H(f)\\vert^2=\\Phi\_{gg}(f)$ | ![image-20210406103335482](../../Typora.assets/image-20210406103335482.png) |
| stochastisch | $\\varphi(\\tau)\\circledast \\varphi\_{hh}(\\tau)=\\varphi\_{gg}(\\tau)$ | $N_0·\\vert H(f)\\vert^2=\\Phi\_{n_fn_f}(f)$ | ![image-20210406103355715](../../Typora.assets/image-20210406103355715.png) |

| Störspektrum Φ~nn~ im Bereich f~1~ bis f~2~ |  | Tiefpassgefiltert |
|---------------------------------------------|--|-------------------|
| $\\overline{\\epsilon_1^2}=2\\int\_{f_1}^{f_2}\\Phi\_{nn}(f)~df$ | $N=\\Phi\_{nn}·(f_2-f_1)$ | $\\Phi\_{nn}=N·\\frac1{8f_g}$ |

<div style="page-break-after: always; break-after: page;"></div>

## 4\. FIR-Filter $z=e^{j\\omega\\Delta T}$

| $\\displaystyle Y^*(\\omega)=\\Delta T\\sum\_{m=-\\infty}^\\infty y_me^{-j\\omega m\\Delta T}$ | $y_m=\\sum\_{i=0}^Nb_iu\_{m-i}$ | $F(z)=\\dfrac{b_0z^N+b_1z^{N-1}+…+b_N}{z^N}$ |
|---------------------------------------------------------------------------------------|------------------------------|---------------------------------------------|
| $\\displaystyle F^*(\\omega)=\\sum\_{i_0}^Nb_ie^{-j\\omega i\\Delta T}$ | N Nullstellen<br />N-fache Polstelle | $\\displaystyle F(z)=\\sum\_{i=0}^Nb_iz^{-i}=\\sum\_{i=0}^Nb_iz^{N-i}·\\dfrac1{z^N}$ |

$f_g\\le\\dfrac{f\_{\\Delta T}}2=\\dfrac1{2\\Delta T}$			$f_0=\\dfrac{arg(z)}{2\\pi}·f\_{\\Delta T}$

| Impulsantwort |  |
|---------------|--|
| ![image-20210510101328315](../../Typora.assets/image-20210510101328315.png) | $arg(z)=\\varphi=\\bigg{\\begin{matrix}\\arctan(\\frac yx)&x>0\\\\pm90°&x=0\\\\arctan(\\frac yx)\\pm180°&x<0\\end{matrix}$ |

|  | Nullstellen | Pole |
|--|-------------|------|
| $\\vert F(e^{j\\omega})\\vert=\\dfrac{\\text{Abstand zu Nullstellen}}{\\text{Abstand zu Polen}}$ | $z_0=e^{j\\omega_0\\Delta T}=e^{j\\frac{f_0}{f\_{\\Delta T}}2\\pi}$ |  |
|  | $F(z_0)→0$ |  |

| Normierte Frequenz | maximale Kreisfrequenz |  |  |
|--------------------|------------------------|--|--|
| $\\Omega=\\omega·\\Delta T$ | $\\omega\_{max}=\\dfrac\\pi{\\Delta T}$ | $\\Omega\_{max}=\\pi$ |  |

|  | $F(\\omega)=e^{-j\\omega\\Delta T}$ | $F(\\omega)=2·\\cos(\\omega\\Delta T)$ | $F(\\omega)=2j·\\sin(\\omega\\Delta T)$ |
|--|----------------------------------|------------------------------------|-------------------------------------|
| **Betrag** | \![SV4 abs(exp)](../../Typora.assets/SV4 abs(exp).png) | \![SV4 abs(cos)](../../Typora.assets/SV4 abs(cos).png) | \![SV4 abs(sin)](../../Typora.assets/SV4 abs(sin).png) |
| **Phase** | \![SV4 angle(exp)](../../Typora.assets/SV4 angle(exp).png) | \![SV4 angle(cos)](../../Typora.assets/SV4 angle(cos).png) | \![SV4 angle(sin)](../../Typora.assets/SV4 angle(sin).png) |

<div style="page-break-after: always; break-after: page;"></div>

### 4\.4 Linearer Phasengang

Phasensprünge bei Nullstellen

|  | reell | komplex |
|--|-------|---------|
| **auf Einheitskreis** | $z_0$ | $z_0\\quad z_0^*$ |
| **Einheitskreis gespiegelt** | $z_0\\quad1/z_0$ | $z_0\\quad1/z_0\\qquad z_0^*\\quad1/z_0^*$ |

| ( $H(j\\Omega)=0\\quad/\\quad h(t)$ ) | N gerade | N ungerade |
|------------------------------------|----------|------------|
| **symmetrisch** $b_i=b\_{N-i}$ (reell / gerade) | Typ 1 $keine$ | Typ 2 $\\Omega=0\\quad\\Omega=\\pi$ |
| **anti-symmetrisch** $b_i=-b\_{N-i}$ (imaginär / ungerade) | Typ 3 $\\Omega=\\pi$ | Typ 4 $\\Omega=0$ |

<img src="../../Typora.assets/image-20210615114020596.png" alt="image-20210615114020596" style="zoom:33%;" />

| Filtermitte |  | gerade | ungerade |
|-------------|--|--------|----------|
| symmetrisch | $\\Delta \\varphi=(\\frac N2-i)\\omega\\Delta T$ | $\\varphi_m=-\\dfrac N2\\omega\\Delta T$ | $\\varphi_m=\\omega\\Delta T·\\dfrac{n-1}2+\\dfrac{\\Delta T}2$ |
| antisymmetrisch | $\\Delta\\varphi=i·\\omega\\Delta T$ |  |  |

| Bandsperre | Tiefpass | Tiefpass |
|------------|----------|----------|
| $z_1=e^{j\\omega\\Delta T}\\qquad z_2=e^{-j\\omega\\Delta T}$ | $N\\ge\\dfrac1{\\Delta f\\Delta T}-1$ | Nullstelle $z_0=e^{j2\\pi f\\Delta T}$ |

### 4\.6 Filterentwurf

| Schritt | Bedeutung | Formel |
|---------|-----------|--------|
| 1\. Wunschfunktion | nicht-kausale Wunschfunktion (bsp. TP Ω~g~) | $F(\\Omega)$ |
| 2\. Filtertyp | Typ 1-4, je nach |  |
| 3\. unendliche FR | unendlich oft 2π-periodisch wiederholte<br />Fourierreihe (Ω~p~ = ω · ΔT = 2π) | $\\displaystyle h(i)=\\dfrac1{2\\pi}\\int\_{-\\pi}^\\pi F^*(\\Omega)·e^{ji\\Omega}~d\\Omega$ |
| 4\. begrenzte FR | nicht-kausales, endliches Filter | $\\displaystyle F^*(\\Omega)=\\sum\_{-N/2}^{N/2}h(i)·e^{-ji\\Omega}$ |
| 5\. kausales Filter |  | $b_i=h\\left(i-\\frac{N}2\\right)$ |

| maximale Flankenbreite $\\Delta f\_{max}$ | $N\\ge\\dfrac1{\\Delta f\_{max}·\\Delta T}$ |
|-----------------------------------------|----------------------------------------|

<div style="page-break-after: always; break-after: page;"></div>

### 4\.8 Hilbert-Transformator

| nicht-kausal | kausal | Frequenzgang | verzögert |
|--------------|--------|--------------|-----------|
| $h(i\\Delta T)=\\bigg{\\begin{matrix}\\frac2{i·\\pi}&i=\\pm1,3,5,..\\0&i=\\pm2,4,6,..\\end{matrix}$ | $b_i=h\\left(i-\\frac{N}2\\right)$ | $H(\\Omega)=\\dfrac{U^+(\\Omega)}{X^*(\\Omega)}$ | $X^*\_v(\\Omega)=X^*(\\Omega)·e^{-j\\frac N2\\Omega}$ |

<img src="../../Typora.assets/image-20210615130833453.png" alt="image-20210615130833453" style="zoom:33%;" />

# 5\. IIR-Filter

| Zeitbereich | $\\displaystyle a_0~y[{\\scriptstyle m}]=\\sum\_{j=0}^Mb_j~u[{\\scriptstyle m-j}]\-\\sum\_{i=1}^Na_i~y[{\\scriptstyle m-i}]$ | $a~y[{\\scriptstyle n+2}]\+b~y[{\\scriptstyle n+1}]\+c~y[{\\scriptstyle n}]\\\\qquad=d~x[{\\scriptstyle n+1}]\+e~x[{\\scriptstyle n}]$ |
|-------------|---------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------|
| **Übertragungs-funktion** | $F(z)=\\dfrac{Y(z)}{U(z)}=\\dfrac{\\sum\_{j=0}^M b_j~z^{-j}}{\\sum\_{i=0}^N a_i~z^{-i}}$ | $\\dfrac{Y(z)}{X(z)}=\\dfrac{d~z+e}{a~z^2+b~z+c}$ |
| **Frequenzbereich** | $\\displaystyle a_0~Y(z)=\\sum\_{j=0}^Mb_j~U(z)·z^{-j}-\\sum\_{i=1}^Na_i~Y(z)·z^{-i}$ | <br />**stabiler Pol** |
| **Variable** | $s=\\sigma+j\\omega\\qquad z=e^{\\sigma~\\Delta T}·e^{j\\omega~\\Delta T}$ | $\\sigma<0\\qquad \\vert z\\vert <1$ |

<img src="../../Typora.assets/signal-flow-graph-of-the-difference-equation-n.jpg" alt="https://image.slideserve.com/296000/signal-flow-graph-of-the-difference-equation-n.jpg" style="zoom: 50%;" />

## 5\.4 Entwurfsverfahren

| Rechteck-Integration | Bilineare Transformation |  |
|----------------------|--------------------------|--|
| $\\dot x(t)=\\dfrac{x[m]\-x[m-1]}{\\Delta T}$ | $s=\\dfrac2{\\Delta T}\\dfrac{z-1}{z+1}$ |  |

<div style="page-break-after: always; break-after: page;"></div>

### 5\.5 Kanonische Form

<img src="../../Typora.assets/image-20210615164543849.png" alt="image-20210615164543849" style="zoom: 30%;" />

## 6\. Matlab

```matlab
y = filter(b,a,u)		% b/a: Koeffizienten Zähler/Nenner	u: Eingangsvektor

freqz(b,a)				% plottet Frequenzgang
[H,w] = freqz(b,a,m)	% Frequenzgang an m Punkten berechnen
H = freqz(b,a,w)		% Frequenzgang an Werten in Vektor w
abs(H)  /  angle(H)		
stem(b)					% Impulsantwort

sys = tf(zähler, nenner)
sys = zpk(zeros,poles,gain)
sys = filt(zähler, nenner)	% diskrete Übertragungsfunktion

b = fir1(N,x)			% Tiefpass 	N: Ordnung	x=fg·2ΔT: Grenzfrequenz
```

```matlab
x = [1 2; 3 4]				% Matrix: 	1.Zeile [1 2]	2.Zeile [3 4]
x = 1:2:7					% Matrix:	1.Zeile [1 3 5 7]	(default space: 1)
x = linspace(1, 7, 4)		% Matrix:	1.Zeile [1 3 5 7]	(first, last, count)
x = x'						% Zeilen- zu Spaltenvektor:	1.Spalte [1 3 5 7]

rand(y,x)					% Matrix: y-Zeilen, x-Spalten (Zufällige Werte zw. 0 1)
randn(y,x)					% Matrix: y-Zeilen, x-Spalten (Zufällige Werte ca. zw. -4/+4)
zeros(y,x)	/  ones(y,x)	% Matrix: y-Zeilen, x-Spalten (Nullen / Einsen)
size(A)						% neue Matrix:	[y, x]	(enthält Zeilen/Spalten-Anzahl von A)

x(2) / x(2:4)				% 2. / 2. bis 4. Element des Vektors
A(y, x)						% Element in Zeile y und Spalte x (: für ganze Zeile/Spalte)
```